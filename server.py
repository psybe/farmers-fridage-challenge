from flask import Flask, render_template, request, jsonify
from pymongo import MongoClient
from random import randint
import math
import datetime

CLIENT = MongoClient('mongodb://admin:admin@ds245357.mlab.com:45357/farmers_fridge')
DB = CLIENT.farmers_fridge
LAYOUTS = DB.layouts
app = Flask(__name__)

doc_length = LAYOUTS.count()


@app.route('/')
def hello_world():
    index = int(math.floor(randint(1, doc_length)))
    layout = LAYOUTS.find()[index]
    date = datetime.datetime.now().isoformat()
    LAYOUTS.update_one(
        {"_id": layout['_id']},
        {
            "$push":
            {
                "visits":
                {
                    "time": date,
                    "clicked": False
                }
            }
        }, upsert=False)
    return render_template('home.html', title=layout['title'], button=layout['button'], id=layout['_id'])


@app.route('/api/v1/layouts', methods=['POST'])
def post():
    header = request.args.get('header')
    date_clicked = request.args.get('time')
    LAYOUTS.update_one(
        {"header": header},
        {
            "$push":
            {
                "visits":
                {
                    "time": date_clicked,
                    "clicked": True
                }
            }
        }, upsert=False)
    return jsonify({'response': 'success'})


app.run(debug=True)
